package domain;

public class Message {
    private User from, to;
    private String text;
    private boolean seen;
    private int id;
    static int id_gen = 0;


    public void setId(int id) {
        this.id = id;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public void setTo(User to) {
        this.to = to;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public User getFrom() {
        return from;
    }

    public User getTo() {
        return to;
    }

    public String getText() {
        return text;
    }

    public void setSeen(boolean b) {
        this.seen = b;
    }

    public boolean isSeen() {
        return seen;
    }

    public Message () {
        id_gen ++;
        setId(id_gen);
    }

    public Message (User from, User to, String text) {
        this();
        setFrom(from);
        setTo(to);
        setText(text);
        setSeen(false);
    }

    public Message (int id, User from, User to, String text, boolean seen) {
        setId(id);
        setFrom(from);
        setTo(to);
        setText(text);
        setSeen(seen);
    }

    @Override
    public String toString() {
        return "Message id: " + id + "from " + from.getUsername() + " to " + to.getUsername() + "Text: " + text;
    }
}
