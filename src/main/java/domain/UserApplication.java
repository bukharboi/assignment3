package domain;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Set;


public class UserApplication {
    // users - a list of users
    public ArrayList<User> userList = new ArrayList<User>();
    private ArrayList<Admin> adminList = new ArrayList<Admin>();
    private Scanner sc = new Scanner(System.in);
    private User signedUser;
    private Admin signedAdmin;
    private boolean adminHere = false;
    private int size = 0;
    static java.util.TreeMap<String, Integer> map = new java.util.TreeMap<String, Integer>();

    public ArrayList<User> getList() {
        return userList;
    }

    public void start() throws IOException {
        pushDataAgain();
        adminPushDataAgain();
        magicWithFriends();
        // fill userlist from db.txt

        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } if (choice == 2) {
                break;
            } else if (choice == 999) {
                System.out.println("Hello admin");
                adminHere = true;
                adminMenu ();
            }
        }

        logOff();
        saveUserlist();
        saveAdminlist();

        // save the userlist to db.txt -> его нету так как он сразу сохраняет в файл
    }

    void adminSettings (int pr) {
        if (pr >= 1) {
            System.out.println("1. change user info");
        }
        if (pr >= 2) {
            System.out.println("2. delete user info");
        }
        if (pr >= 3) {
            System.out.println("3. manage other admins");
        }

        int choice = sc.nextInt();
        if (choice == 1) {
            adminChangeInfo();
        } else if (choice == 2 && pr >= 2) {
            adminDeleteUser();
        } else if (choice == 3 && pr >= 3) {
            adminManageOtherAdmins();
        }
    }
    private void adminChangeInfo () {
        for (int i = 0; i < size; i ++) {
            User curUser = userList.get(i);
            System.out.println(i + ": " + curUser);
        }

        int ch = sc.nextInt();
        if (ch >= size) {
            System.out.println("ERROR");
        } else {
            System.out.println("What data you want change?");
            System.out.println("1. Name Surname");
            System.out.println("2. Username");
            System.out.println("3. Password");
            System.out.println("4. Exit");
            int choice = sc.nextInt();
            User curUser = userList.get (ch);
            String now = curUser.getPassword().getPasswordStr();
            if (choice == 1) {
                changeName(curUser, now);
            } else if (choice == 2) {
                changeUsername(curUser, now);
            } else if (choice == 3) {
                changePassword(curUser, now);
            } else {
                return;
            }
        }
    }
    private void adminDeleteUser () {
        for (int i = 0; i < size; i ++) {
            User cur = userList.get(i);
            System.out.println(i + ": " + cur);
        }

        int ch = sc.nextInt();
        if (ch >= size) {
            System.out.println("ERROR");
        } else {
            System.out.println("Please enter your password");
            String check = sc.next();
            if (check.equals(signedAdmin.getPassword().getPasswordStr()) == true) {
                userList.remove(ch);
                size--;
            }
        }
    }
    private void adminManageOtherAdmins () {
        System.out.println("Please enter your password");
        String check = sc.next();
        if (check.equals(signedAdmin.getPassword().getPasswordStr()) == false) {
            System.out.println("ERROR");
            return;
        }

        int signedId = -1;

        for (int i = 0; i < adminList.size(); i ++) {
            Admin cur = adminList.get(i);
            if (cur == signedAdmin) {
                signedId = i;
                continue;
            }
            System.out.println(i + ": " + cur);
        }
        int x = sc.nextInt();
        if (x == signedId) {
            System.out.println("ERROR");
            return;
        }
        if (x >= adminList.size()) {
            System.out.println("ERROR");
        } else {
            System.out.println("1. Change admin privilege");
            System.out.println("2. Kill admin");
            int ch = sc.nextInt();
            if (ch == 1) {
                int s = sc.nextInt();
                Admin cur = adminList.get(x);
                cur.setPrivilege(s);
            } else {
                adminList.remove(x);
            }
        }
    }

    private void adminMenu () throws IOException {
        while (true) {
            if (signedAdmin == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            }
            else {
                System.out.println("Admin, you are signed in.");
                System.out.println("1. Data about account");
                System.out.println("2. LogOff");
                System.out.println("3. Exit");
                System.out.println("4. Admin settings");

                System.out.println("");
                int choice = sc.nextInt();

                if (choice == 1) signedAdmin.userProfile();
                else if (choice == 2) logOff();
                else if (choice == 3) break;
                else {
                    adminSettings(signedAdmin.getPrivilege());
                }
            }
        }
    }

    private void menu() throws IOException {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            }
            else {
                System.out.println("You are signed in.");
                System.out.println("1. Data about account");
                System.out.println("2. LogOff");
                System.out.println("3. Change my data");
                System.out.println("4. Wanna new friend");
                System.out.println("5. Write new message to my friend");
                System.out.println("6. Check chatbox");
                System.out.println("7. Exit");
                int choice = sc.nextInt();

                if (choice == 1) signedUser.userProfile();
                else if (choice == 2) logOff();
                else if (choice == 3) changeData();
                else if (choice == 4) newFriend();
                else if (choice == 5) writeNewMessage();
                else if (choice == 6) checkChatbox();
                else break;
            }
        }
    }
    private void authentication() throws IOException {
        // sign in
        System.out.println("1. Sign in ?");
        System.out.println("2. Sign up ?");
        // sign up
        int choice = sc.nextInt();
        if (choice == 1) {
            signIn();
        } else {
            signUp();
        }
    }
    private void logOff() {
        signedUser = null;
    }

    private void signIn() throws IOException {
        String login = null;
        while (true) {
            System.out.println("Please enter login...");
            login = sc.next();
            if (login == null) continue;
            else break;
        }
        String password;
        while (true) {
            System.out.println("Please enter password...");
            password = sc.next();
            if (password == null) continue;
            else break;
        }


        for (int i = 0; i < size; i ++) {
            User now = userList.get(i);
            if (now.getUsername().equals(login) && now.getPassword().getPasswordStr().equals(password)) {
                signedUser = now;
                break;
            }
        }


        if (signedUser == null) {
            for (int i = 0; i < adminList.size(); i ++) {
                Admin now = adminList.get(i);
                if (now.getUsername().equals(login) && now.getPassword().getPasswordStr().equals(password)) {
                    signedAdmin = now;
                    return;
                }
            }
            System.out.println("Incorrect try");
            System.out.println("1. Try again");
            System.out.println("2. Forget password?");
            System.out.println("3. Sign up");

            int choice = sc.nextInt();
            if (choice == 1) {
                signIn();
            } else if (choice == 2) {
                forgetPassword ();
            } else {
                signUp();
            }
        } else {
            menu();
        }
    }
    private void signUp() throws IOException {
        System.out.println("Your name?");
        String name = sc.next();
        System.out.println("Your surname?");
        String surname = sc.next();
        String username;
        Password password;
        while (true) {
            System.out.println("Enter username:");
            String tmpUsername = sc.next();
            if (map.containsKey(tmpUsername)) {

                System.out.println("sorry this username is already used :(");
                System.out.println("1. Need some suggestions?");
                System.out.println("2. Enter myself");
                int choice = sc.nextInt();
                if (choice == 1) {
                    String tmptmpuname = suggestUsername();
                    System.out.println(tmptmpuname);
                    System.out.println("Do you like it?");
                    System.out.println("1. yes");
                    System.out.println("2. no");
                    choice = sc.nextInt();
                    if (choice == 1) {
                        username = tmptmpuname;
                        break;
                    } else {
                        continue;
                    }
                } else {
                    continue;
                }
            } else {
                System.out.println("Nice choice :)");
                username = tmpUsername;
                break;
            }
        }

        System.out.println("Enter password");
        System.out.println("Criteria:");
        System.out.println("1. password should contain at least 9 chars");
        System.out.println("2. it should contain at least one uppercase char");
        System.out.println("3. it should contain at least one lowercase char");
        System.out.println("4. it should contain at least one digit");
        while (true) {
            String tmpPassword = sc.next();
            Password ps = new Password(tmpPassword);
            if (ps.isCorrect(tmpPassword) == true) {
                System.out.println("Nice choice :)");
                ps = new Password(tmpPassword);
                password = ps;
                break;
            } else {
                System.out.println("sorry please reread the criteria and enter password again");
                System.out.println("Enter password");
                continue;
            }
        }
        System.out.println("Admin secret code?");
        System.out.println("if you don't know please enter 0");
        System.out.println("if you don't know and enter otherwise I will ban you");
        int x = sc.nextInt();
        if (x == 20628) {
            System.out.println("HELLO NEW ADMIN");
            Admin newAdmin = new Admin(userList.get(size-1).getId()+1, name, surname, username, password, 1);
            addAdmin(newAdmin);
            if (adminHere == true) {
                adminMenu();
            }
        } else if (x != 0) {
            System.out.println("ERROR");
            return;
        }

        User newUser = new User (userList.get(size-1).getId()+1, name, surname, username, password);
        addUser(newUser);
//        File file = new File("C:\\Users\\magzhan\\Desktop\\E-LEARNING\\JAVA OOP\\week2\\assigment2\\db.txt");
//        String nowstr = newUser.getId() + " " + newUser.getName() + " " + newUser.getSurname() + " " + newUser.getUsername() + " " + newUser.getPassword().getPasswordStr();
//        PrintWriter filewriter = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
//        filewriter.println("");
//        filewriter.println(nowstr);
//        filewriter.flush();
//        filewriter.close();
    }

    private void changeData () {
        System.out.println("What data you want change?");
        System.out.println("1. Name Surname");
        System.out.println("2. Username");
        System.out.println("3. Password");
        System.out.println("4. Exit");
        int choice = sc.nextInt();
        System.out.println("Please enter your last password");
        String now = sc.next();
        if (choice == 1) {
            changeName(signedUser, now);
        } else if (choice == 2) {
            changeUsername(signedUser, now);
        } else if (choice == 3) {
            changePassword(signedUser, now);
        } else {
            return;
        }
    }
    private void changeUsername (User signedUser, String now) {
        if (signedUser.getPassword().getPasswordStr().equals(now)) {
            System.out.println("ok. now enter your new username");
            String now2 = sc.next();
            if (map.containsKey(now2) == false) {
                map.remove(signedUser.getUsername(), true);
                map.put(now2, 1);
                signedUser.setUsername(now2);
            } else {
                System.out.println("sorry this username is already used :(");
                System.out.println("1. Need some suggestions?");
                System.out.println("2. Enter myself");
                int choice = sc.nextInt();
                if (choice == 1) {
                    String tmptmpuname = suggestUsername();
                    System.out.println(tmptmpuname);
                    System.out.println("Do you like it?");
                    System.out.println("1. yes");
                    System.out.println("2. no");
                    choice = sc.nextInt();
                    //goto l;
                    if (choice == 1) {
                        map.remove(signedUser.getUsername(), true);
                        map.put(tmptmpuname, 1);
                        signedUser.setUsername(tmptmpuname);
                        return;
                    } else {
                        changeUsername(signedUser, now);
                    }
                } else {
                    changeUsername(signedUser, now);
                }
            }
        } else {
            System.out.println("ERROR");
        }
    }
    private void changeName (User signedUser, String now) {
        if (signedUser.getPassword().getPasswordStr().equals(now)) {
            System.out.println("ok. now enter your new name and surname");
            String now2 = sc.next();
            String now3 = sc.next();
            signedUser.setName(now2);
            signedUser.setSurname(now3);
            return;
        } else {
            System.out.println("ERROR");
        }
    }
    private void changePassword (User signedUser, String now) { ;
        if (signedUser.getPassword().getPasswordStr().equals(now)) {
            System.out.println("Enter password");
            System.out.println("Criteria:");
            System.out.println("1. password should contain at least 9 chars");
            System.out.println("2. it should contain at least one uppercase char");
            System.out.println("3. it should contain at least one lowercase char");
            System.out.println("4. it should contain at least one digit");
            String password;
            while (true) {
                String tmpPassword = sc.next();
                Password ps = new Password(tmpPassword);
                if (ps.isCorrect(tmpPassword) == true) {
                    System.out.println("Nice choice :)");
                    password = tmpPassword;
                    break;
                } else {
                    System.out.println("sorry please reread the criteria and enter password again");
                    System.out.println("Enter password");
                    continue;
                }
            }
            signedUser.getPassword().setPasswordStr(password);
        } else {
            System.out.println("ERROR");
        }
    }

    private void forgetPassword() throws IOException {
        //File file = new File("C:\\Users\\magzhan\\Desktop\\E-LEARNING\\JAVA OOP\\week2\\assigment2\\db.txt"); // file with users
        //Scanner fileScanner = new Scanner(file);
        System.out.println("Please enter your full name, surname and username");
        String name = sc.next();
        String surname = sc.next();
        String username = sc.next();
        String password = null;

        for (int i = 0; i < size; i ++) {

            int tmpId;
            String tmpName, tmpSurname, tmpUsername, tmpPassword;
            tmpId = userList.get(i).getId();
            tmpName = userList.get(i).getName();
            tmpSurname = userList.get(i).getSurname();
            tmpUsername = userList.get(i).getUsername();
            tmpPassword = userList.get(i).getPassword().getPasswordStr();

            if (tmpName.equals(name) == true && tmpSurname.equals(surname) == true && tmpUsername.equals(username) == true) {
                password = tmpPassword;
            }
        }

        if (password == null) {
            System.out.println("In our database there is no person similar to your data. MB you have some mistakes in your input.");
            System.out.println("1. Rewrite?");
            System.out.println("2. Sign up");
            int choice = sc.nextInt();
            if (choice == 1) {
                forgetPassword();
            } else {
                signUp();
            }
        } else {
            System.out.println("Please save it somewhere ;)");
            System.out.println("Your password is: ");
            System.out.println(password);
            signIn();
        }
    }
    private User getUserById (int id) {
        User tmp = null;
        for (int i = 0; i < size; i ++) {
            User curUser = userList.get(i);
            if (curUser.getId() == id) {
                tmp = curUser;
                break;
            }
        }
        for (int i = 0; i < adminList.size(); i ++) {
            User curUser = adminList.get(i);
            if (curUser.getId() == id) {
                tmp = curUser;
                break;
            }
        }
        return tmp;
    }

    private void addUser(User user) throws IOException {
        //System.out.println(user.getName() + " " + user.getSurname() + " " + user.getUsername() + " " + user.getPassword().getPasswordStr());
        if (size == 0) {
            size++;
            map.put(user.getUsername(), 1);
            userList.add(user);
        } else {
            map.put(user.getUsername(), 1);
            userList.add(size, user);
            size++;
        }
    }

    private void addAdmin (Admin admin) throws IOException {
        map.put(admin.getUsername(), 1);
        adminList.add(adminList.size(), admin);
    }
    private void newFriend() {
        System.out.println("How do you wanna find your friend?");
        System.out.println("1. By Name/Surname");
        System.out.println("2. By Username");
        int choice = sc.nextInt();
        if (choice == 1) {
            while (true) {
                System.out.println("Please input your friend Name/Surname");
                String myFriendName = sc.next();
                String myFriendSurname = sc.next();

                ArrayList<User> possibleUserList = new ArrayList<>();
                for (int i = 0; i < userList.size(); i++) {
                    User tmp = userList.get(i);
                    if (tmp == signedUser) continue;
                    if (tmp.getName().equals(myFriendName) == true && tmp.getSurname().equals(myFriendSurname) == true) {
                        possibleUserList.add(tmp);
                    }
                }
                System.out.println("Possible friend's number is" + possibleUserList.size());
                for (int i = 0; i < possibleUserList.size(); i++) {
                    User curUser = possibleUserList.get(i);
                    System.out.println(i + 1 + " -> " +
                            "username: " + curUser.getUsername() +
                            " name: " + curUser.getName() +
                            " surname: " + curUser.getSurname()
                    );
                }

                System.out.println("Which one is your friend?");
                System.out.println("If no one please input 0");

                choice = sc.nextInt();
                if (choice != 0) {
                    signedUser.addFriend(possibleUserList.get(choice - 1));
                    break;
                } else {
                    System.out.println("oh darling...");
                    System.out.println("Do you want input again?");
                    System.out.println("1. yes");
                    System.out.println("2. no i wanna exit");
                    choice = sc.nextInt();
                    if (choice == 1) {
                        continue;
                    } else {
                        break;
                    }
                }
            }
        } else {
            while (true) {
                System.out.println("Please enter username of your friend: ");
                String myFriend = sc.next();
                User curUser = null;
                for (int i = 0; i < userList.size(); i++) {
                    User tmp = userList.get(i);
                    if (tmp == signedUser) continue;
                    if (tmp.getUsername().equals(myFriend) == true) {
                        curUser = tmp;
                        break;
                    }
                }

                if (curUser == null) {
                    System.out.println("Ohh... there is no such kind of user");
                    System.out.println("1. Do you want type again?");
                    System.out.println("2. Exit");
                    choice = sc.nextInt();
                    if (choice == 1) {
                        continue;
                    } else {
                        break;
                    }
                } else {
                    signedUser.addFriend(curUser);
                    break;
                }
            }
        }
    }
    private void magicWithFriends() {
        for (int i = 0; i < userList.size(); i ++) {
            User curUser = userList.get(i);
            curUser.magicFriends();
        }
    }
    static String suggestUsername() {
        int length = (int) ((int)(Math.random() * 1000) % 10 + 1);
        String res = null;
        for (int i = 1; i <= length; i++) {
            res += (char) ((int) ((int)(Math.random() * 1000) % 26) + 'a');
        }
        if (map.containsKey(res) == true) {
            return suggestUsername();
        } else {
            return res;
        }
    }

    private void writeNewMessage() {
        System.out.println("To which friend do you want write?");
        signedUser.showFriends();
        System.out.println("Input 0 to exit");
        while (true) {
            int choice = sc.nextInt();
            if (choice == 0) {
                return;
            } else {
                User myFriend = signedUser.getFriendById(choice);
                //System.out.println(myFriend);
                if (myFriend == null) {
                    System.out.println("There is no guy with such kind of id");
                    System.out.println("Do you wanna re-enter?");
                    System.out.println("1. Yes");
                    System.out.println("2. No I wanna EXIT");

                    choice = sc.nextInt();
                    if (choice == 2) {
                        break;
                    } else {
                        continue;
                    }
                } else {
                    System.out.println("Your message: ");
                    String text = sc.next();
                    //System.out.println(text);
                    Message newMessage = new Message (signedUser, myFriend, text);
                    signedUser.addOutMessage (newMessage);
                    myFriend.addIncomeMessage (newMessage);
                    break;
                }
            }
        }
    }
    private void checkChatbox() {
        ArrayList<Message> cur = signedUser.inMessage;
        int id = 1;
        //System.out.println(cur.size());
        for (int i = 0; i < cur.size(); i ++) {
            Message nowMessage = cur.get(i);
            //System.out.println(nowMessage.isSeen());
            if (nowMessage.isSeen() == false) {
                System.out.println(id + "- New message from " + nowMessage.getFrom().getUsername());
                id ++;
            }
        }

        while (true) {
            System.out.println("Which one open?");
            System.out.println("type 0 to exit");
            int choice = sc.nextInt();
            if (choice == 0) {
                return;
            } else {
                Message curMessage = null;
                id = 1;
                for (int i = 0; i < cur.size(); i++) {
                    Message nowMessage = cur.get(i);
                    //System.out.println(nowMessage);
                    if (nowMessage.isSeen() == false) {
                        if (id == choice) {
                            curMessage = nowMessage;
                            break;
                        }
                        id++;
                    }
                }
                if (curMessage == null) {
                    System.out.println("Sorry there is no such kind of guy");
                    System.out.println("Do you wanna re-enter?");
                    System.out.println("1. Yes");
                    System.out.println("2. No please let me exit");
                    choice = sc.nextInt();
                    if (choice == 1) {
                        continue;
                    } else {
                        break;
                    }
                } else {
                    System.out.println(curMessage.getText());
                    curMessage.setSeen(true);
                    System.out.println("Do you want to answer?");
                    System.out.println("1. Yes");
                    System.out.println("2. No");

                    choice = sc.nextInt();
                    if (choice == 1) {
                        System.out.println("Your message: ");
                        String text = sc.next();
                        //System.out.println(text);
                        Message newMessage = new Message (signedUser, curMessage.getFrom(), text);
                        signedUser.addOutMessage (newMessage);
                        curMessage.getFrom().addIncomeMessage (newMessage);
                        break;
                    } else {

                    }
                }

                System.out.println("Do you want to check chatbox again?");
                System.out.println("1. Yes");
                System.out.println("2. No please let me exit");
                choice = sc.nextInt();
                if (choice == 1) {
                    continue;
                } else {
                    break;
                }
            }
        }
    }
    private void pushDataAgain() throws IOException {
        File file = new File("C:\\Users\\magzhan\\Desktop\\E-LEARNING\\JAVA OOP\\week2\\assigment2\\db.txt");
        Scanner scc = new Scanner(file);

        while (scc.hasNext()) {
            String tmpName, tmpSurname, tmpUsername, tmpPassword;
            int tmpId = scc.nextInt();
            tmpName = scc.next();
            tmpSurname = scc.next();
            tmpUsername = scc.next();
            tmpPassword = scc.next();
            //System.out.println(tmpId + " " + tmpName + " " + tmpSurname + " " + tmpUsername + " " + tmpPassword);
            addUser(new User (tmpId, tmpName, tmpSurname, tmpUsername, new Password(tmpPassword)));
        }

        file = new File("C:\\Users\\magzhan\\Desktop\\E-LEARNING\\JAVA OOP\\week2\\assigment2\\db_friends.txt");
        scc = new Scanner(file);

        while (scc.hasNextInt()) {
            int curId = scc.nextInt();
            User curUser = getUserById(curId);
            if (curUser != null) {
                while (scc.hasNextInt()) {
                    int importance, friendId;
                    importance = scc.nextInt();
                    if (importance != -1) {
                        friendId = scc.nextInt();
                        User friendUser = getUserById(friendId);
                        curUser.addFriend(friendUser, importance);
                    } else {
                        break;
                    }
                }
            }
            //System.out.println("lol");
        }

        file = new File("C:\\Users\\magzhan\\Desktop\\E-LEARNING\\JAVA OOP\\week2\\assigment2\\db_message_in.txt");
        scc = new Scanner(file);

        while (scc.hasNextInt()) {
            int curId = scc.nextInt();
            while (scc.hasNextInt()) {
                int MessageId = scc.nextInt();
                if (MessageId == -1)
                    break;
                int fromUserId = scc.nextInt();
                int toUserId = scc.nextInt();
                String text = scc.next();
                int x = scc.nextInt();
                boolean seen = (x == 1 ? true : false);
                User from = getUserById(fromUserId);
                User to = getUserById(toUserId);
                //System.out.println(MessageId + " " + from.getUsername() + " " + to.getUsername() + " " + text + " " + seen);
                from.addOutMessage(new Message (MessageId, from, to, text, seen));
                to.addIncomeMessage(new Message (MessageId, from, to, text, seen));
            }
        }
    }
    private void adminPushDataAgain() throws IOException {
        File file = new File("C:\\Users\\magzhan\\Desktop\\E-LEARNING\\JAVA OOP\\week2\\assigment2\\admin_db.txt");
        Scanner scc = new Scanner(file);

        while (scc.hasNext()) {
            String tmpName, tmpSurname, tmpUsername, tmpPassword;
            int tmpId = scc.nextInt();
            tmpName = scc.next();
            tmpSurname = scc.next();
            tmpUsername = scc.next();
            tmpPassword = scc.next();
            int tmpPr = scc.nextInt();
            //System.out.println(tmpId + " " + tmpName + " " + tmpSurname + " " + tmpUsername + " " + tmpPassword);
            Admin ad = new Admin (tmpId, tmpName, tmpSurname, tmpUsername, new Password(tmpPassword), tmpPr);
            addAdmin(ad);
            //ad.userProfile();
        }
    }
    private void saveUserlist () throws IOException {
        String lol = "";
        File file = new File("C:\\Users\\magzhan\\Desktop\\E-LEARNING\\JAVA OOP\\week2\\assigment2\\db.txt");
        Files.write (Paths.get(String.valueOf(file)), lol.getBytes());
        for (int i = 0; i < size; i ++) {
            User newUser = userList.get(i);
            String nowStr = newUser.getId() + " " + newUser.getName() + " " + newUser.getSurname() + " " + newUser.getUsername() + " " + newUser.getPassword().getPasswordStr();
            fileWriter(file, nowStr);
        }

        file = new File("C:\\Users\\magzhan\\Desktop\\E-LEARNING\\JAVA OOP\\week2\\assigment2\\db_friends.txt");
        Files.write (Paths.get(String.valueOf(file)), lol.getBytes());
        for (int i = 0; i < size; i ++) {
            User newUser = userList.get(i);
            String nowStr = newUser.getId() + " ";
            nowStr += newUser.myFriendId();
            fileWriter(file, nowStr);
        }

        file = new File("C:\\Users\\magzhan\\Desktop\\E-LEARNING\\JAVA OOP\\week2\\assigment2\\db_message_in.txt");
        Files.write (Paths.get(String.valueOf(file)), lol.getBytes());
        for (int i = 0; i < size; i ++) {
            User cur = userList.get(i);
            ArrayList<Message> nowMessage = cur.inMessage;
            String res = "";
            for (int j = 0; j < nowMessage.size(); j ++) {
                Message curMessage = nowMessage.get(j);
                res +=
                        curMessage.getId() +
                        " " + curMessage.getFrom().getId() +
                        " " + curMessage.getTo().getId() +
                        " " + curMessage.getText() +
                        " " + (curMessage.isSeen() == true ? 1 : 0) + " ";
            }
            res += "-1";
            res = cur.getId() + " " + res;
            fileWriter(file, res);
        }
    }
    private void saveAdminlist () throws IOException {
        String lol = "";
        File file = new File("C:\\Users\\magzhan\\Desktop\\E-LEARNING\\JAVA OOP\\week2\\assigment2\\admin_db.txt");
        Files.write (Paths.get(String.valueOf(file)), lol.getBytes());
        for (int i = 0; i < adminList.size(); i ++) {
            Admin newUser = adminList.get(i);
            String nowStr = newUser.getId() + " " + newUser.getName() + " " + newUser.getSurname() + " " + newUser.getUsername() + " " + newUser.getPassword().getPasswordStr() + " " + newUser.getPrivilege();
            fileWriter(file, nowStr);
        }
    }
    void fileWriter (File a, String b) throws IOException {
        PrintWriter fileWriter = new PrintWriter(new BufferedWriter(new FileWriter(a, true)));
        fileWriter.println("");
        fileWriter.println(b);
        fileWriter.flush();
        fileWriter.close();
    }

}
