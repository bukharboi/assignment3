package domain;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class User {
    private int id;
    private String name, surname, username;
    private Password password;
    static int id_gen = 0;

    static java.util.TreeMap<Integer, Integer> map = new java.util.TreeMap<Integer, Integer>();
    public Set <Pair <Integer, User > > friendList = new HashSet<Pair <Integer, User> >();

    public ArrayList<Message> outMessage = new ArrayList<Message>();
    public ArrayList<Message> inMessage = new ArrayList<Message>();

    public void addFriend (User myFriend, int importance) {
        Set <Pair <Integer, User > > tmp = new HashSet<Pair <Integer, User> >();
        for (Pair <Integer, User> to: friendList) {
            if (to.getFirst() < importance) {
                tmp.add(to);
            } else {
                tmp.add(new Pair<Integer, User>(to.getFirst() + 1, to.getSecond()));
            }
        }
        friendList = tmp;
        friendList.add(new Pair <Integer, User>(importance, myFriend));
    }
    public void addFriend (User myFriend) {
        int importance = Math.min (5, friendList.size() + 1);
        Set <Pair <Integer, User > > tmp = new HashSet<Pair <Integer, User> >();
        for (Pair <Integer, User> to: friendList) {
            if (to.getFirst() < importance) {
                tmp.add(to);
            } else {
                tmp.add(new Pair<Integer, User>(to.getFirst() + 1, to.getSecond()));
            }
        }
        friendList = tmp;
        friendList.add(new Pair <Integer, User>(importance, myFriend));
    }
    public String myFriendId () {
        String res = "";
        if (friendList.size() > 0) {
            for (Pair<Integer, User> to : friendList) {
                int curId = to.getSecond().getId();
                res += to.getFirst() + " " + curId;
                res += " ";
            }
        }

        res += " -1";
        return res;
    }
    public void magicFriends () {
        for (Pair<Integer, User> to: friendList) {
            User myFriend = to.getSecond();
            boolean was = false;
            for (Pair<Integer, User> toto: myFriend.friendList) {
                if (toto.getSecond() == this) {
                    was = true;
                    break;
                }
            }

            if (was == true)
                map.put(myFriend.getId(), 1);
        }
    }

    public void addOutMessage(Message cur) {
        outMessage.add (cur);
    }
    public void addIncomeMessage(Message cur) {
        inMessage.add (cur);
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setPassword(Password password) {
        this.password = password;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public int getId() {
        return id;
    }
    public Password getPassword() {
        return password;
    }
    public String getUsername() {
        return username;
    }

    public User (int id, String name, String surname, String username, Password password) throws FileNotFoundException {
        setId(id);
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }
    public void userProfile () {
        System.out.println("Username: " + username);
        System.out.println("Surname: " + surname);
        System.out.println("Name: " + name);

        System.out.println("");
        System.out.println("");
        showFriends();
    }
    public void showFriends() {
        System.out.println("Number of friends: " + friendList.size());
        int Id = 1;
        for (Pair<Integer, User> to: friendList) {
            System.out.println(
                    "Id: " + Id +
                    " Importance: " + to.getFirst() +
                    " username:" + to.getSecond().getUsername() +
                    " name:" + to.getSecond().getName() +
                    " surname:" + to.getSecond().getSurname() +
                    ((map.containsKey(to.getSecond().getId()) == true) ? " confirmed friend" : " not confirmed friend")
            );
            Id ++;
        }
    }
    public User getFriendById (int Id) {
        int nowId = 1;
        System.out.println("asdoinasdinaisodioasdnasd");
        for (Pair <Integer, User> to: friendList) {
            //System.out.println("auibsianioasdnoadsionasdionasdoindsa " + to.getSecond());
            if (nowId == Id) {
                return to.getSecond();
            }
            nowId ++;
        }
        return null;
    }

    @Override
    public String toString() {
        return id  + " " + name + " " + surname + " " + username + " " + password + " " + password.getPasswordStr();
    }
}
