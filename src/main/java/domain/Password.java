package domain;

public class Password {
    // passwordStr // it should contain uppercase and lowercase letters and digits
    // and its length must be more than 9 symbols
    private String passwordStr;

    public void setPasswordStr(String passwordStr) {
        this.passwordStr = passwordStr;
    }

    public Password (String password) {
        setPasswordStr(password);
    }

    public String getPasswordStr() {
        return passwordStr;
    }

    public boolean isCorrect (String a) {
        if ((int)a.length() < 9) {
            return false;
        }
        boolean upper = false, lower = false, dig = false;
        for (int i = 0; i < (int)a.length(); i ++) {
            if ('A' <= a.charAt(i) && a.charAt(i) <= 'Z') {
                upper = true;
            }
            if ('a' <= a.charAt(i) && a.charAt(i) <= 'z') {
                lower = true;
            }
            if ('0' <= a.charAt(i) && a.charAt(i) <= '9') {
                dig = true;
            }
        }

        if (upper == true && lower == true && dig == true) {
            return true;
        }
        return false;
    }
}
