package domain;

import java.io.FileNotFoundException;

public class Admin extends User {
    private int privilege;

    public void setPrivilege(int privilege) {
        this.privilege = privilege;
    }

    public int getPrivilege() {
        return privilege;
    }

    public Admin(int id, String name, String surname, String username, Password password, int privilege) throws FileNotFoundException {
        super(id, name, surname, username, password);
        setPrivilege(privilege);
    }

    public void userProfile () {
        System.out.println("Username: " + getUsername());
        System.out.println("Surname: " + getSurname());
        System.out.println("Name: " + getName());
        System.out.println("Privilege: " + privilege);
        System.out.println("");
        System.out.println("");
        showFriends();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
